import { varFadeInLeft } from "@components/animate";
import Link from "@components/customs/Link";
import ThemeModeMenu from "@components/ThemeModeMenu";
import { NavigateBefore } from "@mui/icons-material";
import { Box, Button, Stack, Typography } from "@mui/material";
import { motion } from "framer-motion";
import { useRouter } from "next/router";
import React from "react";

export default function Navbar() {
    const { pathname } = useRouter();
    const isLandingPage = pathname === "/";

    return (
        <Stack
            direction={"row"}
            justifyContent="space-between"
            alignItems={"center"}
            sx={{
                position: "fixed",
                top: 0,
                right: 0,
                height: "60px",
                width: "100vw",
                backgroundColor: "primary.main",
                px: 4,
                zIndex: 1000,
                transition: "all 0.3s ease-in-out",
            }}
        >
            {!isLandingPage && (
                <Link href={"/"}>
                    <Stack
                        direction={"row"}
                        alignItems={"center"}
                        spacing={2}
                        sx={{ cursor: "pointer" }}
                    >
                        <Box component={motion.div} {...varFadeInLeft}>
                            <Button
                                variant="text"
                                sx={{
                                    backgroundColor: "white",
                                    boxShadow: 12,
                                    transition: "all 0.3s ease-in-out",
                                    "&:hover": {
                                        opacity: 0.8,
                                        backgroundColor: "white",
                                    },
                                }}
                                startIcon={<NavigateBefore />}
                            >
                                Back
                            </Button>
                        </Box>
                    </Stack>
                </Link>
            )}
            <Link href={"/"}>
                <Stack
                    direction={"row"}
                    alignItems={"center"}
                    spacing={2}
                    sx={{ cursor: "pointer" }}
                >
                    <Box
                        component={"img"}
                        src="favicon/logo.png"
                        sx={{ height: "25px", width: "25px" }}
                    />
                    <Typography variant="h5" color="primary.contrastText">
                        Automata
                    </Typography>
                </Stack>
            </Link>
            <ThemeModeMenu />
        </Stack>
    );
}
