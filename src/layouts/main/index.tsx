import LoadingScreen from "@components/LoadingScreen";
import { MainContextProvider } from "@contexts/MainContext";
import { Box, Container, Typography } from "@mui/material";
import { uniqueId } from "lodash";
import { ReactNode, useEffect, useState } from "react";
import FaToolbar from "./FaToolbar";
import Navbar from "./Navbar";

export default function Layout({ children }: { children: ReactNode }) {
    const [loading, setLoading] = useState(true);

    const getUserID = async () => {
        const currentID = localStorage.getItem("user_id");
        if (!currentID) {
            const userID = uniqueId();
            localStorage.setItem("user_id", userID);
        }
        await new Promise((resolve) => setTimeout(resolve, 1000));
        setLoading(false);
    };

    useEffect(() => {
        getUserID();
    }, []);

    return (
        <MainContextProvider>
            {loading ? (
                <Box
                    sx={{
                        width: "100vw",
                        height: "100vh",
                        zIndex: 100,
                        left: 0,
                        top: 0,
                        position: "fixed",
                        backgroundColor: "background.paper",
                    }}
                >
                    <LoadingScreen />
                    <Typography
                        variant="caption"
                        color="GrayText"
                        align="center"
                    >
                        For every Finit Automaton that you have created will
                        store in the toolbar on your right screen.
                    </Typography>
                </Box>
            ) : (
                <>
                    <Navbar />
                    <Box sx={{ height: "60px", width: "100%" }} />
                    <Container maxWidth="xl">{children}</Container>
                    <Box sx={{ height: "60px", width: "100%" }} />
                    <FaToolbar />
                </>
            )}
        </MainContextProvider>
    );
}
