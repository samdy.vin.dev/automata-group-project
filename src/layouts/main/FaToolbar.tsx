import { MotionInView, varFadeInRight } from "@components/animate";
import FACard from "@components/customs/FACard";
import LoadingScreen from "@components/LoadingScreen";
import { useMainContext } from "@contexts/MainContext";
import {
    NavigateBeforeRounded,
    NavigateNextRounded,
    RefreshRounded,
} from "@mui/icons-material";
import {
    Box,
    Button,
    IconButton,
    Paper,
    Stack,
    Theme,
    Tooltip,
    Typography,
    useMediaQuery,
} from "@mui/material";
import { apiUrl } from "@utils/config";
import axios from "axios";
import { AnimatePresence, motion } from "framer-motion";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { FA } from "src/models";

export default function FaToolbar() {
    const [open, setOpen] = useState(true);
    const isMobile = useMediaQuery((theme: Theme) =>
        theme.breakpoints.down("md"),
    );
    const { refreshFa, fa, setFa, loading, setLoading } = useMainContext();

    const onDeleteFAHandler = async (id: string) => {
        try {
            const res = await axios.delete(apiUrl + `/api/v1/fa/${id}`, {
                headers: { user_id: localStorage.getItem("user_id") },
            });
            if (res.data.payload) {
                toast.success("Deleted!");
            }
            const removedFa = fa!.findIndex((fa: any) => fa.id === id);
            if (removedFa !== -1) {
                setFa([
                    ...fa!.slice(0, removedFa),
                    ...fa!.slice(removedFa + 1),
                ]);
            }

            toast.success("Deleted!");
        } catch (error: any) {
            toast.error(error.message);
        }
    };

    useEffect(() => {
        refreshFa();

        return () => {};
    }, []);

    const RenderFA = () => {
        return (
            <Stack
                spacing={2}
                sx={{
                    p: 2,
                }}
            >
                {fa && fa.length > 0 ? (
                    fa!.map((fa: FA) => (
                        <FACard
                            key={fa.id}
                            fa={fa}
                            onDelete={onDeleteFAHandler}
                        />
                    ))
                ) : (
                    <Stack
                        justifyContent="center"
                        alignItems="center"
                        sx={{ width: "100%", height: "calc(100vh - 250px)" }}
                    >
                        <Box
                            component="img"
                            src="static/empty_state.svg"
                            sx={{ width: "100px", height: "100px" }}
                        />
                        <Typography variant="caption" color="GrayText">
                            You don't have any FA.
                        </Typography>
                    </Stack>
                )}
            </Stack>
        );
    };

    return (
        <Stack
            direction="row"
            alignItems={"flex-start"}
            sx={{
                position: "fixed",
                right: 0,
                top: "80px",
                height: "calc(100vh - 120px)",
                transform: `translateX(${open ? "0" : "270px"})`,
                zIndex: 1000,
            }}
        >
            {!open && (
                <Button
                    onClick={() => setOpen(!open)}
                    sx={{
                        backgroundColor: "primary.main",
                        height: "50px",
                        borderRadius: "20px 0 0 20px",
                        "&:hover": {
                            backgroundColor: "primary.main",
                        },
                        color: "white",
                    }}
                    startIcon={
                        <NavigateBeforeRounded sx={{ color: "white" }} />
                    }
                >
                    FA
                </Button>
            )}
            {open && !isMobile && (
                <Button
                    onClick={() => setOpen(!open)}
                    sx={{
                        backgroundColor: "primary.main",
                        height: "50px",
                        borderRadius: "20px 0 0 20px",
                        "&:hover": {
                            backgroundColor: "primary.main",
                        },
                        color: "white",
                    }}
                    startIcon={<NavigateNextRounded sx={{ color: "white" }} />}
                >
                    Close
                </Button>
            )}
            <Paper
                elevation={6}
                sx={{
                    width: "270px",
                    height: "100%",
                    borderRadius: isMobile ? "20px 0 0 20px" : "0 0 0 20px",
                    overflowY: "auto",
                }}
            >
                {open && (
                    <Stack
                        direction="row"
                        sx={{
                            borderRadius: !isMobile ? "0" : "20px 0 0 0",
                            backgroundColor: "primary.darker",
                            opacity: 0.9,
                            height: "50px",
                            position: "sticky",
                            top: "0",
                            left: 0,
                            zIndex: 100,
                        }}
                        alignItems="center"
                    >
                        {isMobile && (
                            <Button
                                onClick={() => setOpen(!open)}
                                sx={{
                                    backgroundColor: "primary.main",
                                    height: "50px",
                                    borderRadius: " 20px 0 0 0",
                                    "&:hover": {
                                        backgroundColor: "primary.main",
                                    },
                                    color: "white",
                                }}
                                startIcon={
                                    <NavigateNextRounded
                                        sx={{ color: "white" }}
                                    />
                                }
                            >
                                Close
                            </Button>
                        )}
                        <Stack flex={1} alignItems="center">
                            <Typography
                                variant="subtitle2"
                                sx={{ color: "white" }}
                            >
                                All Your Designed FA
                            </Typography>
                        </Stack>
                        <Tooltip title="Refresh">
                            <IconButton onClick={async () => await refreshFa()}>
                                <RefreshRounded sx={{ color: "white" }} />
                            </IconButton>
                        </Tooltip>
                    </Stack>
                )}
                {loading ? (
                    <Box sx={{ width: "100%", height: "100%" }}>
                        <LoadingScreen />
                    </Box>
                ) : (
                    <RenderFA />
                )}
            </Paper>
        </Stack>
    );
}
