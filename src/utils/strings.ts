export function isSetFormat(c: string) {
    const CHAR_CODE_A = 65;
    const CHAR_CODE_Z = 90;
    const CHAR_CODE_AS = 97;
    const CHAR_CODE_ZS = 122;
    const CHAR_CODE_0 = 48;
    const CHAR_CODE_9 = 57;
    const CHAR_CODE_COMMA = 44;
    const CHAR_CODE_SPACE = 32;

    const code = c.charCodeAt(0);

    if (
        (code >= CHAR_CODE_A && code <= CHAR_CODE_Z) ||
        (code >= CHAR_CODE_AS && code <= CHAR_CODE_ZS) ||
        (code >= CHAR_CODE_0 && code <= CHAR_CODE_9) ||
        (code >= CHAR_CODE_COMMA &&
            code <= CHAR_CODE_COMMA &&
            code != CHAR_CODE_SPACE)
    ) {
        return true;
    }

    return false;
}

export function validateSetFormat(s: string) {
    for (let i = 0; i < s.length; i++) {
        if (!isSetFormat(s[i])) {
            return false;
        }
    }

    return true;
}
