export const required_features = [
    { name: "DESIGN FA", link: "/design-fa", disable: false },
    { name: "CHECK FA", link: "/check-fa", disable: false },
    { name: "ACCEPT STRING", link: "/accept-string", disable: false },
    {
        name: "CONVERT NFA TO DFA",
        link: "/convert-nfa-to-dfa",
        disable: false,
    },
    { name: "MINIMIZE DFA", link: "/minimize-dfa", disable: false },
];
export const extra_features = [
    { name: "DFA", link: "/dfa", disable: false },
    { name: "NFA", link: "/nfa", disable: true },
];
export const members = [
    {
        name: "Manuth Vann",
        profile: "static/members/manuthvann.jpg",
        url: "https://web.facebook.com/mama.nuth.92",
    },
    {
        name: "Rithiya Vibol",
        profile: "static/members/rithiyavibol.jpg",
        url: "https://web.facebook.com/profile.php?id=100012045405689",
    },
    {
        name: "Panha You",
        profile: "static/members/panhayou.jpg",
        url: "https://web.facebook.com/panha.yuu",
    },
    {
        name: "Samdy Vin",
        profile: "static/members/samdyvin.jpg",
        url: "https://web.facebook.com/samdyvinen/",
    },
    {
        name: "Kim Chou Cheu",
        profile: "static/members/KimChouCheu.jpg",
        url: "https://web.facebook.com/people/Kim-Chou-Cheu/100011139615832/",
    },
];
