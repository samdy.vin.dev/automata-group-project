import RenderFAContent from "@components/customs/RenderFAContent";
import ResponsiveDialog from "@components/customs/ResponsiveDialog";
import { Button } from "@mui/material";
import { apiUrl } from "@utils/config";
import axios from "axios";
import { createContext, useContext, ReactNode, useState } from "react";

export const mainContext = createContext<any>(null);

export const useMainContext = () => useContext(mainContext);

export const MainContextProvider = ({ children }: { children: ReactNode }) => {
    const [fa, setFa] = useState<any>(null);
    const [loading, setLoading] = useState(true);
    const [selectedFa, setSelectedFa] = useState<any>(false);

    const refreshFa = async () => {
        setLoading(true);
        const res = await axios.get(apiUrl + "/api/v1/fa", {
            headers: { user_id: localStorage.getItem("user_id") },
        });
        setFa(res.data.payload);
        setLoading(false);
    };

    const contextValues = {
        refreshFa,
        fa,
        setFa,
        loading,
        setLoading,
        selectedFa,
        setSelectedFa,
    };
    return (
        <mainContext.Provider value={contextValues}>
            {children}
            <ResponsiveDialog
                open={!!selectedFa}
                setOpen={setSelectedFa}
                title={"Finite Automaton (FA) Details"}
                content={!!selectedFa && <RenderFAContent data={selectedFa} />}
                cancelButton={
                    <Button
                        color="inherit"
                        variant="outlined"
                        onClick={() => setSelectedFa(false)}
                    >
                        Close
                    </Button>
                }
            />
        </mainContext.Provider>
    );
};
