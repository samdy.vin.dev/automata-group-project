import { computeDFAEndWith } from "./endWith";
import { computeDFANotEndWith } from "./notEndWith";
import { computeDFAStartWith } from "./startWith";

export enum DFA_COMPUTE_METHOD {
    START_WITH = "Start with",
    NOT_START_WITH = "Not start with",
    END_WITH = "End with",
    NOT_END_WITH = "Not end with",
    INCLUDE = "Include",
    NOT_INCLUDE = "Not include",
    ACCEPT = "Accept",
    NOT_ACCEPT = "Not accept",
}

export const DFA_COMPUTE = {
    startWith: computeDFAStartWith,
    endWith: computeDFAEndWith,
    notEndWith: computeDFANotEndWith,
};
