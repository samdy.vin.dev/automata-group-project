export const computeDFANotEndWith = (_data: DFA_DATA): Promise<DFA_RESULT> => {
    return new Promise((resolve, reject) => {
        const target = _data.target;
        const operator = _data.operator;
        const set = _data.set.split(",");
        const states: DFA_STATE[] = [];
        const transition_function: DFA_TRANSITION_FUNCTION = [];
        const final_states: DFA_STATE[] = [];

        // Initialize states
        states[0] = {
            is_final: false,
            label: "Σ",
            name: "q0",
            next_state: null,
            previous_state: null,
            self_state: "",
        };

        for (let i = 0; i < target!.length; i++) {
            const char = target!.charAt(i);

            const new_state = {
                is_final: false,
                label: (states[i].label + char).replace("Σ", ""),
                name: `q${i + 1}`,
                next_state: null,
                previous_state: states[i],
                self_state: ``,
            };

            states[i + 1] = new_state;

            if (new_state.label === target) {
                states[i + 1].is_final = true;
                final_states.push(new_state);
            }
        }

        // Initialize transition function
        for (let i = 0; i < states.length; i++) {
            transition_function[i] = [];
            const state = states[i];

            for (let j = 0; j < set.length; j++) {
                transition_function[i][j] = [];
                transition_function[i][j][0] = states[0].name;
                const char = set[j];
                const targetLabel = state.label + char;
                // console.log("==============================");
                // console.log("Char:", char);

                for (let k = 0; k < states.length; k++) {
                    const compare_state = states[k];

                    // console.table({
                    //     i,
                    //     j,
                    //     state: state.name,
                    //     char: char,
                    //     targetLabel: targetLabel,
                    // });

                    for (let l = 0; l < targetLabel.length; l++) {
                        const compare_sequence = targetLabel.substring(l);

                        // console.log("--------------------------");
                        // console.table({
                        //     state: compare_state.label,
                        //     compare_sequence,
                        // });
                        if (compare_state.label === compare_sequence) {
                            transition_function[i][j][0] = compare_state.name;
                            break;
                        }
                        // console.log(
                        //     `Match:`,
                        //     compare_state.label === compare_sequence,
                        // );
                    }
                }
            }
        }
        const reverse_states = states.map(
            (state) =>
                ({
                    ...state,
                    is_final: !state.is_final,
                } as DFA_STATE),
        );

        const node_data: DFA_NODE_DATA[] = reverse_states.map(
            (state: DFA_STATE) => ({
                key: state.name,
                text: state.name,
                color: "rgba(0,0,0,0)",
                strokeWidth: state.is_final ? 4 : 1,
            }),
        );
        const link_data: DFA_LINK_DATA[] = [];
        for (let i = 0; i < transition_function.length; i++) {
            for (let j = 0; j < transition_function[i].length; j++) {
                link_data.push({
                    from: reverse_states[i].name,
                    to: transition_function[i][j][0],
                    text: set[j],
                });
            }
        }

        // reverse_states.forEach((state, index) => {
        //     console.log(`${state.name}: ${state.label}`);
        // });
        // console.table(reverse_states);
        // console.table(transition_function);
        // console.table(node_data);
        // console.table(link_data);

        const result: DFA_RESULT = {
            target,
            operator,
            set,
            states: reverse_states,
            transition_function,
            start_state: reverse_states[0],
            final_states: reverse_states.filter((state) => state.is_final),
            node_data,
            link_data,
        };
        resolve(result);
    });
};
