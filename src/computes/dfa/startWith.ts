import { hasIn } from "lodash";

export const computeDFAStartWith = (_data: DFA_DATA): Promise<DFA_RESULT> => {
    return new Promise((resolve, reject) => {
        const target = _data.target;
        const operator = _data.operator;
        const inputs = _data.set.split(",");
        const states: DFA_STATE[] = [];
        const transition_function: string[][][] = [];
        const final_states: DFA_STATE[] = [];

        // Initialize states

        final_states.push(states[target!.length - 1]);

        const result: DFA_RESULT = {
            target,
            operator,
            set: inputs,
            states,
            transition_function,
            start_state: states[0],
            final_states,
            node_data: [],
            link_data: [],
        };
        resolve(result);
    });
};
