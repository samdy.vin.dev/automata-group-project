declare type DFA_DATA = {
    set: string;
    operator: DFA_COMPUTE_METHOD;
    target: string | null;
};

declare type DFA_STATE = {
    name: string;
    is_final: boolean;
    label: string;
    self_state: string;
    next_state: DFA_STATE | null;
    previous_state: DFA_STATE | null;
};

declare type DFA_TRANSITION_FUNCTION = string[][][];

declare type DFA_NODE_DATA = {
    key: string;
    text: string;
    color: string;
    strokeWidth: number;
};

declare type DFA_LINK_DATA = { from: string; to: string; text: string };

declare type DFA_RESULT = {
    operator: DFA_COMPUTE_METHOD;
    target: string | null;
    set: string[];
    states: DFA_STATE[];
    transition_function: DFA_TRANSITION_FUNCTION;
    start_state: DFA_STATE;
    final_states: DFA_STATE[];
    node_data: DFA_NODE_DATA[];
    link_data: DFA_LINK_DATA[];
};
