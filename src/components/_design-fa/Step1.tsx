import React, { useEffect } from "react";
import { useFormikContext } from "formik";
import { FInputContainer, FInput } from "@components/editor/Input";
import { Stack } from "@mui/material";
import { validateSetFormat } from "@utils/strings";

export default function Step1({
    setAllowNextStep,
}: {
    setAllowNextStep: (allowNextStep: boolean) => void;
}) {
    const { values }: any = useFormikContext();

    useEffect(() => {
        const endWithComma = values.symbols[values.symbols.length - 1] === ",";

        if (
            values.symbols.length > 0 &&
            validateSetFormat(values.symbols) &&
            !endWithComma
        ) {
            setAllowNextStep(true);
        } else {
            setAllowNextStep(false);
        }
    }, [values.symbols]);

    return (
        <Stack>
            <FInput label="Symbols" name="symbols" />
        </Stack>
    );
}
