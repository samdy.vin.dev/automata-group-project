import {
    FMultiSelect,
    FMultiSelectAutocomplete,
} from "@components/editor/Input";
import {
    TableContainer,
    Paper,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
} from "@mui/material";
import { useFormikContext } from "formik";
import React, { useEffect } from "react";
import { State } from "src/models";

export default function Step3({
    setAllowNextStep,
}: {
    setAllowNextStep: (allowNextStep: boolean) => void;
}) {
    const { values, setFieldValue }: any = useFormikContext();
    const trans = JSON.parse(values.transitions_json);

    const RenderColumnSelect = ({
        state,
        symbol,
    }: {
        state: State;
        symbol: string;
    }) => {
        const transitionIndex = values.transitions.findIndex(
            (t: any) => t.from === state.name && t.input === symbol,
        );
        if (transitionIndex > -1) {
            return (
                <FMultiSelect
                    options={values.states.map((s: State) => ({
                        label: s.name,
                        value: s.name,
                    }))}
                    label="States"
                    name={`transitions[${transitionIndex}].to`}
                />
            );
        }
        return <i>None</i>;
    };

    return (
        <TableContainer component={Paper}>
            <Table aria-label="fa table">
                <TableHead>
                    <TableRow>
                        <TableCell>States</TableCell>
                        {values.symbols.split(",").map((symbol: string) => (
                            <TableCell align="right">
                                Input&nbsp;({symbol})
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {values.states.map((state: State) => (
                        <TableRow
                            key={state.name}
                            sx={{
                                "&:last-child td, &:last-child th": {
                                    border: 0,
                                },
                            }}
                        >
                            <TableCell component="th" scope="row">
                                {state.name}
                            </TableCell>
                            {values.symbols.split(",").map((symbol: string) => (
                                <TableCell align="right">
                                    <RenderColumnSelect
                                        state={state}
                                        symbol={symbol}
                                    />
                                </TableCell>
                            ))}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
