import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import StepContent from "@mui/material/StepContent";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import React, { useEffect } from "react";
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";
import { useFormikContext } from "formik";
import { LoadingButton } from "@mui/lab";
import { Save } from "@mui/icons-material";
import axios, { AxiosResponse } from "axios";
import { apiUrl } from "@utils/config";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";
import ResponsiveDialog from "@components/customs/ResponsiveDialog";
import RenderFAContent from "@components/customs/RenderFAContent";
import { FA } from "src/models";
import { useMainContext } from "@contexts/MainContext";

const steps = [
    {
        label: "Set Symbols",
        description: `For each Finit Automathon (FA) that you create, you can control how many your symbols would be. For example, if you want to create a Finit Automathon (FA) with 2 symbols from 0 to 1, you can set the symbols as "0,1".`,
    },
    {
        label: "Set States",
        description:
            'An Finit Automathon (FA) contains one or more states which target for single or multiple of input. So at least add one start state and one final state in each Finit Automathon (FA). For example, if you want to create a Finit Automaton (FA) with 2 states, you can set the states as "q0,q1". "q0" is the start state and "q1" is the final state. (The dead state is optional.)',
    },
    {
        label: "Set Transitions",
        description: `Complete the transition function below to define the transition function of Finit Automaton (FA). Note: You can select more than 1 state for each transition. For example, if you want to create a Finit Automaton (FA) which contain a transition that has 2 states, you can set the transition as "q0,q1".`,
    },
];

export default function VerticalLinearStepper() {
    const { refreshFa } = useMainContext();

    const [activeStep, setActiveStep] = React.useState(0);
    const [allowNextStep, setAllowNextStep] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [result, setResult] = React.useState<any>(null);
    const [openResult, setOpenResult] = React.useState(false);
    const { values }: any = useFormikContext();

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    const RenderStep = () => {
        switch (activeStep + 1) {
            case 1:
                return <Step1 setAllowNextStep={setAllowNextStep} />;
            case 2:
                return <Step2 setAllowNextStep={setAllowNextStep} />;
            case 3:
                return <Step3 setAllowNextStep={setAllowNextStep} />;
            default:
                return <></>;
        }
    };

    const onSubmit = async () => {
        setLoading(true);
        try {
            console.log("values", values);
            const body = {
                symbols: values.symbols,
                states: values.states.map((state: any) => ({
                    name: state.name,
                    is_final: state.is_final,
                    is_start: state.is_start,
                    is_dead: state.is_dead,
                })),
                transition_table: {
                    transitions: values.transitions.map((transition: any) => ({
                        from: {
                            name: transition.from,
                        },
                        to: transition.to.map((to: any) => ({
                            name: to,
                        })),
                        input: transition.input,
                    })),
                },
            };
            const res: AxiosResponse<any> = await axios.post(
                apiUrl + "/api/v1/design-fa",
                body,
                {
                    headers: { user_id: localStorage.getItem("user_id") },
                },
            );

            const data = res.data.paylaod;

            setResult({
                title: "Result",
                content: <RenderFAContent data={data as FA} />,
                data: data as FA,
            });
            setOpenResult(true);
            refreshFa();
            toast.success("Saved");
            setLoading(false);
            handleNext();
        } catch (error: any) {
            toast.error(error.message);
            setLoading(false);
        }
    };

    return (
        <Box sx={{ maxWidth: 400 }}>
            <Stepper activeStep={activeStep} orientation="vertical">
                {steps.map((step, index) => (
                    <Step key={step.label}>
                        <StepLabel
                            optional={
                                index === 2 ? (
                                    <Typography variant="caption">
                                        Last step
                                    </Typography>
                                ) : null
                            }
                        >
                            {step.label}
                        </StepLabel>
                        <StepContent>
                            <Typography>{step.description}</Typography>
                            <Box sx={{ my: 2 }}>{RenderStep()}</Box>
                            <Box sx={{ mb: 2 }}>
                                <div>
                                    {activeStep !== steps.length - 1 ? (
                                        <Button
                                            disabled={!allowNextStep}
                                            variant="contained"
                                            onClick={handleNext}
                                            sx={{ mt: 1, mr: 1 }}
                                        >
                                            Continue
                                        </Button>
                                    ) : (
                                        <LoadingButton
                                            variant="contained"
                                            loading={loading}
                                            startIcon={<Save />}
                                            sx={{ mt: 1, mr: 1 }}
                                            onClick={onSubmit}
                                        >
                                            Finish
                                        </LoadingButton>
                                    )}

                                    <Button
                                        disabled={index === 0}
                                        onClick={handleBack}
                                        sx={{ mt: 1, mr: 1 }}
                                    >
                                        Back
                                    </Button>
                                </div>
                            </Box>
                        </StepContent>
                    </Step>
                ))}
            </Stepper>
            {activeStep === steps.length && (
                <Paper square elevation={0} sx={{ p: 3 }}>
                    <Typography>
                        All steps completed - you&apos;re finished
                    </Typography>
                    <Button onClick={handleReset} sx={{ mt: 1, mr: 1 }}>
                        Design another FA
                    </Button>
                </Paper>
            )}
            <ResponsiveDialog
                open={openResult}
                setOpen={setOpenResult}
                title={result?.title}
                content={result?.content}
                cancelButton={
                    <Button
                        color="inherit"
                        variant="text"
                        onClick={() => setOpenResult(false)}
                    >
                        Close
                    </Button>
                }
            />
        </Box>
    );
}
