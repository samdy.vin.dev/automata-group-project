import {
    MotionInView,
    varFadeInLeft,
    varFadeInRight,
} from "@components/animate";
import { Card, CardActionArea, Grid, Typography, Box } from "@mui/material";
import React from "react";
import { Styles } from "src/types/global";
import Link from "@components/customs/Link";
import { motion } from "framer-motion";

const styles: Styles = {
    Card: {
        boxShadow: 12,
    },
    CardActionArea: {
        height: 268,
        p: 10,
        "&:hover": {
            transition: "all 0.2s ease-in-out",
            backgroundColor: "primary.main",
            color: "primary.contrastText",
        },
    },
};

type Tool = {
    name: string;
    link: string;
    disable: boolean;
};

export default function ToolCard({
    tool,
    index,
}: {
    tool: Tool;
    index: number;
}) {
    const RenderCard = () => {
        return (
            <Card
                sx={{
                    ...styles.Card,
                    cursor: "not-allowed",
                }}
            >
                <CardActionArea
                    disabled={tool.disable}
                    sx={{
                        ...styles.CardActionArea,
                        backgroundColor: tool.disable
                            ? "rgba(0,0,0,0.1)"
                            : "background.paper",
                    }}
                >
                    <Typography
                        variant="body1"
                        fontWeight={"bold"}
                        align="center"
                    >
                        {tool.name}
                    </Typography>
                </CardActionArea>
            </Card>
        );
    };

    return (
        <Grid key={`tool key - ${tool.name}`} item xs={12} md={6}>
            {tool.disable ? (
                <RenderCard />
            ) : (
                <Link href={tool.link}>
                    <RenderCard />
                </Link>
            )}
        </Grid>
    );
}
