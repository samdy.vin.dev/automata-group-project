import React from "react";
import NextLink from "next/link";
import MuiLink from "@mui/material/Link";

export default function Link({
    children,
    href,
    ...other
}: {
    children: React.ReactNode;
    href: string;
} & any) {
    return (
        <NextLink passHref href={href}>
            <MuiLink sx={{ textDecoration: "none" }} {...other}>
                {children}
            </MuiLink>
        </NextLink>
    );
}
