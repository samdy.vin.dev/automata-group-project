import { MotionInView, varFadeInUp } from "@components/animate";
import { Typography } from "@mui/material";
import Head from "next/head";
import React from "react";

export default function PageHeader({ title }: { title: string }) {
    return (
        <>
            <Head>
                <title>{title}</title>
            </Head>
            <MotionInView variants={varFadeInUp}>
                <Typography
                    align="center"
                    variant="h3"
                    color="primary"
                    sx={{ mt: 5, mb: 2 }}
                >
                    {title}
                </Typography>
            </MotionInView>
        </>
    );
}
