import {
    Grid,
    Paper,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography,
} from "@mui/material";
import _ from "lodash";
import React from "react";
import { FA, State, Transition } from "src/models";
import RenderDFADiagram from "./DFADiagram";

export default function RenderFAContent({ data }: { data: FA }) {
    const startNode: DFA_NODE_DATA = {
        key: "start",
        text: "",
        color: "rgba(0,0,0,0)",
        strokeWidth: 0,
    };
    const startLink: DFA_LINK_DATA = {
        from: "start",
        to: "q0",
        text: "start",
    };

    const node_data: DFA_NODE_DATA[] = [
        startNode,
        ...data.states.map((state: State) => {
            return {
                key: state.name,
                text: state.name,
                color: "rgba(0,0,0,0)",
                strokeWidth: state.is_final ? 4 : 1,
            } as DFA_NODE_DATA;
        }),
    ] as DFA_NODE_DATA[];

    const link_data: DFA_LINK_DATA[] = [startLink];
    for (const transition of data.transition_table.transitions) {
        for (const t of transition.to) {
            link_data.push({
                from: transition.from.name,
                to: t.name,
                text: transition.input,
            } as DFA_LINK_DATA);
        }
    }

    const result = {
        symbols: _.sortBy(data.symbols.split(",")).join(","),
        states: _.sortBy(data.states, "name"),
        start_state: data.states.find((x) => x.is_start) || { name: "---" },
        final_states: data.states.filter((x) => x.is_final) || { name: "---" },
        transitions: data.transition_table.transitions,
        node_data: [startNode, ...node_data],
        link_data: link_data,
    } as const;

    const RenderResultRow = ({
        label,
        value,
    }: {
        label: string;
        value: string;
    }) => {
        return (
            <>
                <Grid item xs={5}>
                    <Typography variant="body1">{label}</Typography>
                </Grid>
                <Grid item xs={1}>
                    <Typography variant="body1">:</Typography>
                </Grid>
                <Grid item xs={6} sx={{ overflow: "revert" }}>
                    <Typography variant="body1" color="primary">
                        {value}
                    </Typography>
                </Grid>
            </>
        );
    };

    return (
        <Stack>
            <Grid container spacing={2} sx={{ mb: 2 }}>
                <RenderResultRow
                    label="Symbols"
                    value={`{${result!.symbols}}`}
                />
                <RenderResultRow
                    label="State"
                    value={`{ ${result!.states
                        .map((x: State) => x.name)
                        .join(", ")} }`}
                />

                <RenderResultRow
                    label="Start State"
                    value={`${result!.start_state!.name}`}
                />
                <RenderResultRow
                    label="Final State"
                    value={`{ ${result!.final_states
                        .map((x: State) => x.name)
                        .join(", ")} }`}
                />
            </Grid>

            <Stack spacing={1}>
                <Typography>Transition Function</Typography>
                <TableContainer component={Paper} elevation={6}>
                    <Table size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Present State</TableCell>
                                {result.symbols.split(",").map((input: any) => (
                                    <TableCell
                                        key={`State for Input ${input}`}
                                        align="right"
                                    >
                                        State for Input {input}
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {result.states.map((state: State, i: number) => (
                                <TableRow
                                    key={state.name}
                                    sx={{
                                        "&:last-child td, &:last-child th": {
                                            border: 0,
                                        },
                                    }}
                                >
                                    <TableCell component="th" scope="row">
                                        {state.is_start && "→"}
                                        {state.is_final && "*"}
                                        {state.name}
                                    </TableCell>
                                    {result!.symbols
                                        .split(",")
                                        .map((symbol: string) => (
                                            <TableCell
                                                key={`${state.name} for ${symbol}`}
                                                align="right"
                                            >
                                                {"{" +
                                                    result!.transitions
                                                        .find(
                                                            (x) =>
                                                                x.from.name ===
                                                                    state.name &&
                                                                x.input ===
                                                                    symbol,
                                                        )
                                                        ?.to.map(
                                                            (x: State) =>
                                                                x.name,
                                                        )
                                                        .join(",") +
                                                    "}"}
                                            </TableCell>
                                        ))}
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Stack>
            <RenderDFADiagram
                nodeData={result!.node_data as any}
                linkData={result!.link_data as any}
            />
        </Stack>
    );
}
