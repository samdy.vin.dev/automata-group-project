import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { FA, State } from "src/models";
import _ from "lodash";

export default function FATable({ fa }: { fa: FA }) {
    const getTransition = ({ from, input }: { from: State; input: string }) => {
        const result = fa.transition_table.transitions.find(
            (t) => t.from.id === from.id && t.input === input,
        );
        return result!.to;
    };
    return (
        <TableContainer component={Paper}>
            <Table aria-label="fa table">
                <TableHead>
                    <TableRow>
                        <TableCell>States</TableCell>
                        {_.sortBy(fa.symbols.split(",")).map(
                            (symbol: string) => (
                                <TableCell align="right">
                                    Input&nbsp;({symbol})
                                </TableCell>
                            ),
                        )}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {_.sortBy(fa.states, "name").map((state: State) => (
                        <TableRow
                            key={state.name}
                            sx={{
                                "&:last-child td, &:last-child th": {
                                    border: 0,
                                },
                            }}
                        >
                            <TableCell component="th" scope="row">
                                {state.name}
                            </TableCell>
                            {fa.symbols.split(",").map((symbol: string) => (
                                <TableCell align="right">
                                    {" "}
                                    {"{" +
                                        getTransition({
                                            from: state,
                                            input: symbol,
                                        })
                                            .map((s) => s.name)
                                            .join(", ") +
                                        "}"}
                                </TableCell>
                            ))}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
