import {
    CopyAllRounded,
    DeleteForeverRounded,
    Details,
} from "@mui/icons-material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardHeader from "@mui/material/CardHeader";
import Collapse from "@mui/material/Collapse";
import IconButton, { IconButtonProps } from "@mui/material/IconButton";
import Stack from "@mui/material/Stack";
import { styled } from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import * as React from "react";
import { FA, State } from "src/models";
import { toast } from "react-toastify";
import { CardActions, Grid, Tooltip } from "@mui/material";
import FATable from "./FATable";
import axios from "axios";
import InfoIcon from "@mui/icons-material/Info";
import { apiUrl } from "@utils/config";
import _ from "lodash";
import { useMainContext } from "@contexts/MainContext";

interface ExpandMoreProps extends IconButtonProps {
    expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme, expand }) => ({
    transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest,
    }),
}));

export default function FACard({
    fa,
    onDelete,
}: {
    fa: FA;
    onDelete: (id: string) => Promise<void>;
}) {
    const [expanded, setExpanded] = React.useState(false);
    const { setSelectedFa } = useMainContext();

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    const handleCopyClick = async () => {
        try {
            await navigator.clipboard.writeText(fa.id);
            toast.success("Copied!");
        } catch (err) {
            toast.error("Failed to copy!");
        }
    };

    const handleDeleteClick = async () => onDelete(fa.id);

    const handleDetailsClick = () => setSelectedFa(fa);

    return (
        <Card elevation={6} sx={{ maxWidth: 345 }}>
            <CardHeader
                action={
                    <Stack>
                        <Tooltip title="Details">
                            <IconButton onClick={handleDetailsClick}>
                                <InfoIcon />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Copy ID">
                            <IconButton onClick={handleCopyClick}>
                                <CopyAllRounded />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Delete">
                            <IconButton onClick={handleDeleteClick}>
                                <DeleteForeverRounded color="error" />
                            </IconButton>
                        </Tooltip>
                    </Stack>
                }
                title={<Typography variant="subtitle2">{fa.id}</Typography>}
                subheader={
                    fa.created_at
                        ? new Date(fa.created_at).toLocaleString()
                        : new Date().toLocaleString()
                }
            />
            <CardActions disableSpacing>
                <Typography
                    sx={{ ml: 1 }}
                    variant="body2"
                >{`Symbols: {${_.sortBy(fa.symbols.split(",")).join(
                    ",",
                )}}`}</Typography>
                <ExpandMore
                    expand={expanded}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="show more"
                >
                    <ExpandMoreIcon />
                </ExpandMore>
            </CardActions>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent>
                    <Typography paragraph>Transition Function:</Typography>
                    <FATable fa={fa} />
                </CardContent>
            </Collapse>
        </Card>
    );
}
