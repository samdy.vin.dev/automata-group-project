import { Box, Stack, Typography } from "@mui/material";
import go from "gojs";
import { result } from "lodash";
import { useEffect } from "react";

class DemoForceDirectedLayout extends go.ForceDirectedLayout {
    // Override the makeNetwork method to also initialize
    // ForceDirectedVertex.isFixed from the corresponding Node.isSelected.
    makeNetwork(coll: any) {
        // call base method for standard behavior
        const net = super.makeNetwork(coll);
        net.vertexes.each((vertex) => {
            const node = vertex.node;
            // @ts-ignore
            if (node !== null) vertex.isFixed = node.isSelected;
        });
        return net;
    }
}

const RenderDFADiagram = ({
    nodeData,
    linkData,
}: {
    nodeData: go.ObjectData[];
    linkData: go.ObjectData[];
}) => {
    const initDiagram = () => {
        const $ = go.GraphObject.make;
        const dfaDiagram = $(go.Diagram, "dfa-diagram", {
            initialAutoScale: go.Diagram.Uniform,
            layout: new DemoForceDirectedLayout(),
        }) as go.Diagram;
        const nodeDataArray = nodeData;
        const linkDataArray = linkData;

        dfaDiagram.nodeTemplate = $(
            go.Node,
            "Auto",
            $(
                go.Shape,
                "Circle",
                {
                    fill: "white",
                    stroke: "black",
                },
                new go.Binding("fill", "color"),
                new go.Binding("strokeWidth", "strokeWidth"),
            ),
            $(go.TextBlock, { margin: 5 }, new go.Binding("text", "text")),
        );
        dfaDiagram.linkTemplate = $(
            go.Link,
            { curve: go.Link.Bezier },
            $(
                go.Shape, // the link shape
                { strokeWidth: 1.5 },
            ),
            $(
                go.Shape, // the arrowhead
                { toArrow: "Standard", stroke: null },
            ),
            $(go.TextBlock, new go.Binding("text", "text").makeTwoWay(), {
                segmentOffset: new go.Point(0, 10),
            }),
        );

        dfaDiagram.model = new go.GraphLinksModel(nodeDataArray, linkDataArray);
    };

    useEffect(() => {
        initDiagram();
    }, []);

    return (
        <Stack
            sx={{
                mt: 2,
            }}
            spacing={1}
        >
            <Typography>Finit Automaton Diagram</Typography>
            <Box
                id="dfa-diagram"
                sx={{
                    width: "100%",
                    height: "500px",
                    boxShadow: 6,
                    borderRadius: 1,
                }}
            >
                ...DIAGRAM...
            </Box>

            {/* <Stack
                spacing={2}
                direction="row"
                alignContent={"center"}
                alignItems="center"
            >
                <Typography variant="caption">→q :</Typography>
                <Typography variant="caption">Start State</Typography>
            </Stack>
            <Stack
                spacing={2}
                direction="row"
                alignContent={"center"}
                alignItems="center"
            >
                <Typography variant="caption">*q :</Typography>
                <Typography variant="caption">Final State</Typography>
            </Stack> */}
            <Stack
                spacing={1}
                direction="row"
                alignContent={"center"}
                alignItems="center"
                sx={{ pt: 1 }}
            >
                <Box
                    sx={{
                        width: "35px",
                        height: "35px",
                        borderRadius: "35px",
                        border: "4px solid black",
                    }}
                />
                <Typography variant="caption">Final State</Typography>
            </Stack>
        </Stack>
    );
};

export default RenderDFADiagram;
