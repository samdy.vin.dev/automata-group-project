import { FA } from "./fa.entity";
import { Transition } from "./transitions.entity";

export class TransitionTable {
    id!: string;

    transitions!: Transition[];

    fa!: FA;

    created_at!: Date;
    updated_at!: Date;
}
