import { State } from "./states.entity";
import { TransitionTable } from "./transition_tables.entity";

export class Transition {
    id!: string;

    from!: State;

    to!: State[];

    input!: string;

    transition_table!: TransitionTable;

    created_at!: Date;
    updated_at!: Date;
}
