import { State } from "./states.entity";
import { TransitionTable } from "./transition_tables.entity";

export class FA {
    id!: string;

    symbols!: string;

    states!: State[];

    transition_table!: TransitionTable;

    user_id!: string;

    created_at!: Date;
    updated_at!: Date;
}
