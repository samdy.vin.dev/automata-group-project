import { FA } from "./fa.entity";

export class State {
    id!: string;

    name!: string;

    is_start!: boolean;

    is_final!: boolean;

    is_dead!: boolean;

    fa!: FA;

    created_at!: Date;
    updated_at!: Date;
}
