import { MotionInView, varFadeInUp } from "@components/animate";
import RenderDFADiagram from "@components/customs/DFADiagram";
import PageHeader from "@components/customs/PageHeader";
import {
    FAutocomplete,
    FInput,
    FInputContainer,
} from "@components/editor/Input";
import { NavigateBefore, Settings } from "@mui/icons-material";
import { LoadingButton } from "@mui/lab";
import {
    Box,
    Button,
    Container,
    Divider,
    Grid,
    Paper,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography,
} from "@mui/material";
import { createStyles, makeStyles } from "@mui/styles";
import { Form, Formik } from "formik";
import { AnimatePresence, motion } from "framer-motion";
import go from "gojs";
import Link from "next/link";
import React, { useState } from "react";
import { toast } from "react-toastify";
import { DFA_COMPUTE, DFA_COMPUTE_METHOD } from "src/computes/dfa";
import * as Yup from "yup";

const useStyles = makeStyles(() =>
    createStyles({
        rotateIcon: {
            animation: "$spin 2s linear infinite",
        },
        "@keyframes spin": {
            "0%": {
                transform: "rotate(0deg)",
            },
            "100%": {
                transform: "rotate(360deg)",
            },
        },
    }),
);

export default function DFA() {
    const classes = useStyles();
    const [data, setData] = useState<DFA_DATA | null>(null);
    const [result, setResult] = useState<DFA_RESULT | null>(null);
    const [loading, setLoading] = useState(false);

    const initialValues = {
        target: "",
        set: "",
        operator: null,
    };

    const validationSchema = Yup.object().shape({
        target: Yup.string().required("Task is required"),
        set: Yup.string().required("Input is required"),
        operator: Yup.mixed().required("Operator is required"),
    });

    const computeDfa = async (_data: DFA_DATA): Promise<DFA_RESULT | null> => {
        switch (_data.operator.value) {
            case DFA_COMPUTE_METHOD.START_WITH:
                return await DFA_COMPUTE.startWith(_data);
            case DFA_COMPUTE_METHOD.END_WITH:
                return await DFA_COMPUTE.endWith(_data);
            case DFA_COMPUTE_METHOD.NOT_END_WITH:
                return await DFA_COMPUTE.notEndWith(_data);
            default:
                return null;
        }
    };

    const validateTarget = async (set: string, target: string) => {
        return new Promise((resolve, reject) => {
            let finished = 0;
            let matched = 0;

            const _set = set.split(",");
            const _target = target.split("");

            for (let i = 0; i < _target.length; i++) {
                const char = _target[i];
                const exited = _set.find((s) => s === char);
                if (exited) {
                    matched++;
                }
                finished++;
                if (finished === _target.length) {
                    const validTarget = matched === _target.length;
                    resolve(validTarget);
                }
            }
        });
    };

    const onSubmit = async (values: DFA_DATA) => {
        // Validate Target ...
        const validTarget = await validateTarget(
            values.set,
            values.target as string,
        );

        if (!validTarget) {
            toast.error("Invalid Target, Target must be in Input Set");
            return;
        }

        // Start Computing ...
        setData(values);
        setLoading(true);

        const result = await computeDfa(values);
        const startNode: DFA_NODE_DATA = {
            key: "start",
            text: "",
            color: "rgba(0,0,0,0)",
            strokeWidth: 0,
        };
        const startLink: DFA_LINK_DATA = {
            from: "start",
            to: "q0",
            text: "start",
        };

        result!.node_data = [startNode, ...result!.node_data];
        result!.link_data = [startLink, ...result!.link_data];
        setResult(result);

        // Finish Computing ...
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    };

    const RenderResultRow = ({
        label,
        value,
    }: {
        label: string;
        value: string;
    }) => {
        return (
            <>
                <Grid item xs={5}>
                    <Typography variant="body1">{label}</Typography>
                </Grid>
                <Grid item xs={1}>
                    <Typography variant="body1">:</Typography>
                </Grid>
                <Grid item xs={6}>
                    <Typography variant="body1" color="primary">
                        {value}
                    </Typography>
                </Grid>
            </>
        );
    };

    const RenderResult = () => {
        return (
            <Stack>
                <Typography variant="h5" color="primary" sx={{ mb: 2 }}>
                    Result
                </Typography>

                <Grid container spacing={2} sx={{ mb: 2 }}>
                    <RenderResultRow
                        label="Set"
                        value={`{${result?.set.join(",")}}`}
                    />
                    <RenderResultRow
                        label="Operator"
                        value={`${result?.operator.label}`}
                    />
                    <RenderResultRow
                        label="Target"
                        value={`${result?.target}`}
                    />
                    <RenderResultRow
                        label="State"
                        value={`{${result?.states
                            .map((x: DFA_STATE) => x.name)
                            .join(",")}}`}
                    />

                    <RenderResultRow
                        label="Start State"
                        value={`${result?.start_state.name}`}
                    />
                    <RenderResultRow
                        label="Final State"
                        value={`${result?.final_states
                            .map((x: DFA_STATE) => x.name)
                            .join(",")}`}
                    />
                </Grid>

                <Stack spacing={1}>
                    <Typography>Transition Function</Typography>
                    <TableContainer component={Paper} elevation={6}>
                        <Table
                            sx={{ minWidth: 650 }}
                            size="small"
                            aria-label="a dense table"
                        >
                            <TableHead>
                                <TableRow>
                                    <TableCell>Present State</TableCell>
                                    {result?.set.map((input: any) => (
                                        <TableCell
                                            key={`State for Input ${input}`}
                                            align="right"
                                        >
                                            State for Input {input}
                                        </TableCell>
                                    ))}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {result?.states.map(
                                    (state: DFA_STATE, i: number) => (
                                        <TableRow
                                            key={state.name}
                                            sx={{
                                                "&:last-child td, &:last-child th":
                                                    {
                                                        border: 0,
                                                    },
                                            }}
                                        >
                                            <TableCell
                                                component="th"
                                                scope="row"
                                            >
                                                {state.name === "q0" && "→"}
                                                {state.is_final && "*"}
                                                {state.name}
                                            </TableCell>
                                            {result?.transition_function[i].map(
                                                (item) => (
                                                    <TableCell
                                                        key={item[0]}
                                                        align="right"
                                                    >
                                                        {item[0]}
                                                    </TableCell>
                                                ),
                                            )}
                                        </TableRow>
                                    ),
                                )}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Stack>
                <RenderDFADiagram
                    nodeData={result?.node_data as go.ObjectData[]}
                    linkData={result?.link_data as go.ObjectData[]}
                />
            </Stack>
        );
    };

    return (
        <Container maxWidth="xl">
            <Container maxWidth={"sm"}>
                <PageHeader title="Deterministic Finit Automata (DFA)" />

                <Formik
                    initialValues={initialValues}
                    validateOnBlur={false}
                    validateOnChange={false}
                    validationSchema={validationSchema}
                    onSubmit={onSubmit}
                >
                    {() => (
                        <Form noValidate>
                            <MotionInView variants={varFadeInUp}>
                                <FInputContainer title="Setup DFA Task">
                                    <FInput
                                        name="set"
                                        label="Set"
                                        placeholder="a,b,c,d,e,..."
                                    />
                                    <FAutocomplete
                                        name="operator"
                                        options={Object.values(
                                            DFA_COMPUTE_METHOD,
                                        ).map((value) => ({
                                            label: value,
                                            value: value,
                                        }))}
                                        getOptionDisabled={(option: any) =>
                                            option.value !==
                                                DFA_COMPUTE_METHOD.END_WITH &&
                                            option.value !==
                                                DFA_COMPUTE_METHOD.NOT_END_WITH
                                        }
                                    />
                                    <FInput name="target" placeholder="abc" />
                                    <Stack alignItems="flex-end">
                                        <LoadingButton
                                            type="submit"
                                            variant="contained"
                                            startIcon={<Settings />}
                                            loading={loading}
                                        >
                                            Compute
                                        </LoadingButton>
                                    </Stack>
                                </FInputContainer>
                            </MotionInView>
                        </Form>
                    )}
                </Formik>
            </Container>
            <Container maxWidth={"md"}>
                <AnimatePresence>
                    {data && (
                        <Stack
                            component={motion.div}
                            {...varFadeInUp}
                            key={"Loading State"}
                        >
                            <Divider sx={{ my: 3 }} />
                            <Paper elevation={6} sx={{ p: 2, mb: 5 }}>
                                {loading ? (
                                    <Stack alignItems="center">
                                        <Settings
                                            color="primary"
                                            sx={{ width: "20%", height: "20%" }}
                                            className={classes.rotateIcon}
                                        />
                                    </Stack>
                                ) : (
                                    <RenderResult />
                                )}
                            </Paper>
                        </Stack>
                    )}
                </AnimatePresence>
            </Container>
        </Container>
    );
}
