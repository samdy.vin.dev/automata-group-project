import PageHeader from "@components/customs/PageHeader";
import { FInput, FInputContainer } from "@components/editor/Input";
import { LoadingButton } from "@mui/lab";
import { Button, Container, Stack } from "@mui/material";
import { Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import * as Yup from "yup";
import SettingsIcon from "@mui/icons-material/Settings";
import ResponsiveDialog from "@components/customs/ResponsiveDialog";
import axios from "axios";
import { apiUrl } from "@utils/config";
import { toast } from "react-toastify";
import RenderFAContent from "@components/customs/RenderFAContent";
import { FA } from "src/models";
import { Save } from "@mui/icons-material";
import { useMainContext } from "@contexts/MainContext";

export default function MinimizeDFA() {
    const [result, setResult] = useState<any>(null);
    const [openResult, setOpenResult] = useState(false);
    const [isSaving, setIsSaving] = useState(false);
    const { refreshFa } = useMainContext();

    const initialValues = {
        fa_id: "",
    };

    const validationSchema = Yup.object().shape({
        fa_id: Yup.string().required("Finite Automaton (FA) ID is required"),
    });

    const handleSaveResult = async () => {
        setIsSaving(true);
        const fa = result!.data;

        try {
            const body = {
                symbols: fa.symbols,
                states: fa.states.map((state: any) => ({
                    name: state.name,
                    is_final: state.is_final,
                    is_start: state.is_start,
                    is_dead: state.is_dead,
                })),
                transition_table: {
                    transitions: fa.transition_table.transitions.map(
                        (transition: any) => ({
                            from: {
                                name: transition.from.name,
                            },
                            to: transition.to.map((to: any) => ({
                                name: to.name,
                            })),
                            input: transition.input,
                        }),
                    ),
                },
            };

            const res = await axios.post(apiUrl + "/api/v1/design-fa", body, {
                headers: { user_id: localStorage.getItem("user_id") },
            });
            refreshFa();

            setIsSaving(false);
            setOpenResult(false);
            setTimeout(() => {
                setResult(null);
            }, 500);
            toast.success("Saved");
        } catch (error: any) {
            const message = error.message;
            toast.error(message);
        }
    };

    const onSubmit = async (values: any) => {
        try {
            const body = {
                fa: {
                    id: values.fa_id,
                },
            };
            const res = await axios.post(apiUrl + "/api/v1/minimize-dfa", body);

            const data = res.data.payload.minimizedDfa;

            toast.success("Minimized successfully");

            setResult({
                title: "Result",
                content: <RenderFAContent data={data as FA} />,
                data: data as FA,
            });
        } catch (err: any) {
            const message = err.response.data.message || err.message;
            toast.error(message);
        }
    };

    useEffect(() => {
        if (result) {
            setOpenResult(true);
        }
    }, [result]);

    return (
        <Container maxWidth={"sm"}>
            <PageHeader title="Minimize Deterministic Finite Automaton (DFA)" />
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                validateOnBlur={false}
                validateOnChange={false}
                onSubmit={onSubmit}
            >
                {({ isSubmitting }) => (
                    <Form noValidate>
                        <FInputContainer title="Minimize the Deterministic Finite Automaton (DFA)">
                            <Stack sx={{ mt: 2 }}>
                                <FInput
                                    name="fa_id"
                                    label="Finit Automaton ID"
                                    helperText="Enter Finit Automaton ID that you have created"
                                />
                            </Stack>
                            <Stack direction="row" justifyContent="flex-end">
                                <LoadingButton
                                    loading={isSubmitting}
                                    type="submit"
                                    variant="contained"
                                    startIcon={<SettingsIcon />}
                                >
                                    Minimize
                                </LoadingButton>
                            </Stack>
                        </FInputContainer>
                    </Form>
                )}
            </Formik>
            <ResponsiveDialog
                open={openResult}
                setOpen={setOpenResult}
                title={result?.title}
                content={result && result.content}
                cancelButton={
                    <Button
                        color="inherit"
                        variant="text"
                        onClick={() => setOpenResult(false)}
                    >
                        Cancel
                    </Button>
                }
                submitButton={
                    <LoadingButton
                        loading={isSaving}
                        onClick={handleSaveResult}
                        startIcon={<Save />}
                        variant="contained"
                    >
                        Save
                    </LoadingButton>
                }
            />
        </Container>
    );
}
