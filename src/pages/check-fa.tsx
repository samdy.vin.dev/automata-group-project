import PageHeader from "@components/customs/PageHeader";
import { FInput, FInputContainer } from "@components/editor/Input";
import { LoadingButton } from "@mui/lab";
import { Container, Stack } from "@mui/material";
import { Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import * as Yup from "yup";
import SettingsIcon from "@mui/icons-material/Settings";
import ResponsiveDialog from "@components/customs/ResponsiveDialog";
import axios from "axios";
import { apiUrl } from "@utils/config";
import { toast } from "react-toastify";

export default function CheckFA() {
    const [result, setResult] = useState<any>(null);
    const [openResult, setOpenResult] = useState(false);

    const initialValues = {
        fa_id: "",
    };

    const validationSchema = Yup.object().shape({
        fa_id: Yup.string().required("Finite Automaton (FA) ID is required"),
    });

    const onSubmit = async (values: any) => {
        try {
            const body = {
                fa: {
                    id: values.fa_id,
                },
            };
            const res = await axios.post(apiUrl + "/api/v1/check-fa", body);

            toast.success("Checked successfully");

            setResult({
                title: "Result",
                content:
                    "This is Finit Automaton is a " +
                    (res.data.paylaod.isDFA
                        ? "Deterministic Finite Automaton"
                        : "Non-Deterministic Finite Automaton"),
            });
        } catch (err: any) {
            const message = err.response.data.message || err.message;
            toast.error(message);
        }
    };

    useEffect(() => {
        if (result) {
            setOpenResult(true);
        }
    }, [result]);

    return (
        <Container maxWidth={"sm"}>
            <PageHeader title="Check Finit Automata (FA)" />
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                validateOnBlur={false}
                validateOnChange={false}
                onSubmit={onSubmit}
            >
                {({ isSubmitting }) => (
                    <Form noValidate>
                        <FInputContainer title="Check if the Finit Automaton (FA) is Deterministic Finite Automaton (DFA) or Nondeterministic Finite Automaton (NFA)">
                            <Stack sx={{ mt: 2 }}>
                                <FInput
                                    name="fa_id"
                                    label="Finit Automaton ID"
                                    helperText="Enter Finit Automaton ID that you have created"
                                />
                            </Stack>
                            <Stack direction="row" justifyContent="flex-end">
                                <LoadingButton
                                    loading={isSubmitting}
                                    type="submit"
                                    variant="contained"
                                    startIcon={<SettingsIcon />}
                                >
                                    Check
                                </LoadingButton>
                            </Stack>
                        </FInputContainer>
                    </Form>
                )}
            </Formik>
            <ResponsiveDialog
                open={openResult}
                setOpen={setOpenResult}
                title={result?.title}
                content={result?.content}
            />
        </Container>
    );
}
