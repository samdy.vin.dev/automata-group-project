import { MotionInView, varFadeInUp } from "@components/animate";
import PageHeader from "@components/customs/PageHeader";
import DesignFAFormStepper from "@components/_design-fa/FormStepper";
import { Container, Paper } from "@mui/material";
import { Form, Formik } from "formik";
import React from "react";
import * as Yup from "yup";

export default function DesignFA() {
    const initialValues = {
        symbols: "",
        states: [
            {
                name: "",
                is_final: false,
                is_start: false,
                is_dead: false,
            },
        ],
        transitions: [],
        transitions_json: "",
    };

    const validationSchema = Yup.object().shape({});

    return (
        <Container maxWidth="sm">
            <MotionInView variants={varFadeInUp}>
                <PageHeader title="Design a Finit Automaton (FA)" />
                <Paper elevation={6} sx={{ p: 2 }}>
                    <Formik
                        initialValues={initialValues}
                        enableReinitialize={true}
                        validateOnBlur={false}
                        validateOnChange={false}
                        validationSchema={validationSchema}
                        onSubmit={() => {}}
                    >
                        {() => (
                            <Form noValidate>
                                <DesignFAFormStepper />
                            </Form>
                        )}
                    </Formik>
                </Paper>
            </MotionInView>
        </Container>
    );
}
