import PageHeader from "@components/customs/PageHeader";
import { FInput, FInputContainer } from "@components/editor/Input";
import { LoadingButton } from "@mui/lab";
import { Container, Stack } from "@mui/material";
import { Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import * as Yup from "yup";
import SettingsIcon from "@mui/icons-material/Settings";
import ResponsiveDialog from "@components/customs/ResponsiveDialog";
import axios from "axios";
import { apiUrl } from "@utils/config";
import { toast } from "react-toastify";
import { PlayArrowRounded } from "@mui/icons-material";

export default function AcceptString() {
    const [result, setResult] = useState<any>(null);
    const [openResult, setOpenResult] = useState(false);

    const initialValues = {
        fa_id: "",
        text: "",
    };

    const validationSchema = Yup.object().shape({
        fa_id: Yup.string().required("Finite Automaton (FA) ID is required"),
        text: Yup.string().required("String is required"),
    });

    const onSubmit = async (values: any) => {
        try {
            const body = {
                fa: {
                    id: values.fa_id,
                },
                text: values.text,
            };
            const res = await axios.post(
                apiUrl + "/api/v1/accept-string",
                body,
            );

            toast.success("Checked successfully");

            setResult({
                title: "Result",
                content:
                    "This is String " +
                    (res.data.paylaod.isAccepted ? "Accepted" : "Rejected"),
            });
        } catch (err: any) {
            const message = err.response.data.message || err.message;
            toast.error(message);
        }
    };

    useEffect(() => {
        if (result) {
            setOpenResult(true);
        }
    }, [result]);

    return (
        <Container maxWidth={"sm"}>
            <PageHeader title="Accept String" />
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                validateOnBlur={false}
                validateOnChange={false}
                onSubmit={onSubmit}
            >
                {({ isSubmitting }) => (
                    <Form noValidate>
                        <FInputContainer title="Check if the String is Accept of Reject by the Finit Automaton (FA)">
                            <Stack sx={{ mt: 2 }} spacing={2}>
                                <FInput
                                    name="fa_id"
                                    label="Finit Automaton ID"
                                    helperText="Enter Finit Automaton ID that you have created"
                                />
                                <FInput name="text" label="String" />
                            </Stack>
                            <Stack direction="row" justifyContent="flex-end">
                                <LoadingButton
                                    loading={isSubmitting}
                                    type="submit"
                                    variant="contained"
                                    startIcon={<PlayArrowRounded />}
                                >
                                    Run
                                </LoadingButton>
                            </Stack>
                        </FInputContainer>
                    </Form>
                )}
            </Formik>
            <ResponsiveDialog
                open={openResult}
                setOpen={setOpenResult}
                title={result?.title}
                content={result?.content}
            />
        </Container>
    );
}
