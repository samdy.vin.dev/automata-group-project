import MAvatar from "@components/@material-extend/MAvatar";
import { varFadeInUp } from "@components/animate";
import ToolCard from "@components/customs/ToolCard";
import { Container, Grid, Typography, Box } from "@mui/material";
import {
    extra_features,
    members,
    required_features,
} from "@utils/features.config";
import Link from "@components/customs/Link";
import React from "react";
import { Styles } from "src/types/global";
import { motion } from "framer-motion";

const styles: Styles = {
    Container: {
        alignContent: "center",
        my: 5,
    },

    membersGridContainer: {
        display: "flex",
        alignItems: "center",
        jutifyContent: "center",
    },
    memeberCard: {
        width: "500px",
    },
    memberProfile: {
        height: "100%",
        width: "100%",
        objectFit: "cover",
        boxShadow: 6,
    },
};

export default function index() {
    return (
        <Container maxWidth={"sm"} sx={styles.Container}>
            {/* REQUIRED FEATURES */}
            <Typography
                align="center"
                variant="h3"
                color="primary"
                sx={{ mt: 5, mb: 2 }}
            >
                Required Features
            </Typography>
            <Grid container spacing={2}>
                {required_features.map((tool, i) => (
                    <ToolCard tool={tool} index={i} />
                ))}
            </Grid>

            {/* EXTRA FEATURES */}
            <Typography
                align="center"
                variant="h3"
                color="primary"
                sx={{ mt: 5, mb: 2 }}
            >
                Extra Features
            </Typography>
            <Grid container spacing={2}>
                {extra_features.map((tool, i) => (
                    <ToolCard tool={tool} index={i} />
                ))}
            </Grid>

            <Typography
                align="center"
                variant="h3"
                color="primary"
                sx={{ mt: 5, mb: 2 }}
            >
                Group Member
            </Typography>

            <Grid container spacing={2} sx={styles.membersGridContainer}>
                {members.map((member) => (
                    <Grid
                        key={`member key - ${member.name}`}
                        item
                        xs={6}
                        md={3}
                        sx={styles.memeberCard}
                    >
                        <Link href={member.url} target="blank">
                            <MAvatar
                                sx={styles.memberProfile}
                                src={member.profile}
                            />
                            <Typography
                                variant="body1"
                                align="center"
                                color="GrayText"
                                sx={{ pt: 2 }}
                            >
                                {member.name}
                            </Typography>
                        </Link>
                    </Grid>
                ))}
            </Grid>
        </Container>
    );
}
