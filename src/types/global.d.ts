import { SxProps, Theme } from "@mui/system";

export type Styles = Record<string, SxProps<Theme>>;
